const router = require('express').Router()
const AuthController = require('../app/controllers/auth.controller')
const auth_ctrl = new AuthController()
const uploader = require('../app/middleware/uploader.middleware')
const { post } = require('./user.routes')

router.
post('/login', auth_ctrl.login)
.post('/register', (req, res, next) => {
    req.dir = "public/uploads/user"
    next()
}, uploader.single('image'), auth_ctrl.register)

// post.router('/social-login/:id', auth_ctrl.socialLogin)

module.exports = router