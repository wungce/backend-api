const router = require('express').Router()
const uploader = require('../app/middleware/uploader.middleware')
const BrandController = require('../app/controllers/brand.controller')
const brand_ctr = new BrandController()
const loginCheck = require('../app/middleware/auth.middleware')
const {isAdmin} = require('../app/middleware/rbac.middleware')

const setDest = (req, res, next) => {
    req.dir = 'public/uploads/brand'    
    next()
}
router.route('/')
    .get(brand_ctr. brandList)
    .post(loginCheck, isAdmin, setDest, uploader.single('image'), brand_ctr.addBrand)
router.route('/:id')
    .get(brand_ctr.getById)
    .put(loginCheck, isAdmin, setDest, uploader.single('image'), brand_ctr.updateBrand)
    .delete(loginCheck, isAdmin, brand_ctr.deleteById)
module.exports = router