const router = require('express').Router()
const UserController = require('../app/controllers/user.controller')
const user_ctrl = new UserController()
const uploader = require('../app/middleware/uploader.middleware')

const isAdmin = (req, res, next) => {
    next()
}
router.route('/')
.get(isAdmin, user_ctrl.userList)

router.route('/:id')
.put(uploader.single('image'),user_ctrl.updateUser)
.delete(isAdmin, user_ctrl.userDelete)

module.exports = router