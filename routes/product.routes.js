const router = require('express').Router()
const uploader = require('../app/middleware/uploader.middleware')
const ProductController = require('../app/controllers/product.controller')
const product_ctr = new ProductController()
const loginCheck = require('../app/middleware/auth.middleware')
const {isAdmin} = require('../app/middleware/rbac.middleware')

const setDest = (req, res, next) => {
    req.dir = 'public/uploads/product'    
    next()
}
router.route('/')
    .get(product_ctr. productList)
    .post(loginCheck, isAdmin, setDest, uploader.array('image'), product_ctr.addProduct)
router.route('/:id')
    .get(product_ctr.getById)
    .put(loginCheck, isAdmin, setDest, uploader.array('image'), product_ctr.updateProduct)
    .delete(loginCheck, isAdmin, product_ctr.deleteById)
module.exports = router