const router = require('express').Router()
const uploader = require('../app/middleware/uploader.middleware')
const CategoryController = require('../app/controllers/category.controller')
const category_ctr = new CategoryController()
const loginCheck = require('../app/middleware/auth.middleware')
const {isAdmin} = require('../app/middleware/rbac.middleware')

const setDest = (req, res, next) => {
    req.dir = 'public/uploads/category'    
    next()
}
router.route('/')
    .get(category_ctr. categoryList)
    .post(loginCheck, isAdmin, setDest, uploader.single('image'), category_ctr.addCategory)
router.route('/:id')
    .get(category_ctr.getById)
    .put(loginCheck, isAdmin, setDest, uploader.single('image'), category_ctr.updateCategory)
    .delete(loginCheck, isAdmin, category_ctr.deleteById)
module.exports = router