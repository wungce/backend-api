const router = require('express').Router()
const uploader = require('../app/middleware/uploader.middleware')
const BannerController = require('../app/controllers/banner.controller')
const banner_ctr = new BannerController()
const loginCheck = require('../app/middleware/auth.middleware')
const {isAdmin} = require('../app/middleware/rbac.middleware')

const setDest = (req, res, next) => {
    req.dir = 'public/uploads/banner'    
    next()
}
router.route('/')
    .get(banner_ctr. bannerList)
    .post(loginCheck, isAdmin, setDest, uploader.single('image'), banner_ctr.addBanner)
router.route('/:id')
    .get(banner_ctr.getById)
    .put(loginCheck, isAdmin, setDest, uploader.single('image'), banner_ctr.updateBanner)
    .delete(loginCheck, isAdmin, banner_ctr.deleteById)
module.exports = router