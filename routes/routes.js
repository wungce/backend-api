const express = require('express')
const app = express()
const auth_routes = require('./auth.routes')
const user_routes = require('./user.routes')
const brand_routes = require('./brand.routes')
const banner_routes = require('./banner.routes')
const category_routes = require('./category.routes')
const product_routes = require('./product.routes')
app.use(auth_routes)

app.use('/user', (req, res, next) => {
    req.dir = "public/uploads/user"
    next()
}, user_routes)

app.use('/brand', brand_routes)

app.use('/banner', banner_routes)

app.use('/category', category_routes)

app.use('/product', product_routes)

module.exports = app