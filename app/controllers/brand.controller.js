const BrandService = require('../services/brand.service')
const {deleteImage} = require('../helpers/function')
class BrandController {
    constructor(){
        this.brand_srv = new BrandService()
    }
    addBrand = (req, res, next) => {
        try{
            let data = req.body
            if(req.file) {
                data.image = req.file.filename
            }
            this.brand_srv.validationBrand(data)
            this.brand_srv.createBrand(data)
            res.json({
                msg : "Brand is created.",
                status : true,
                result : data
            })

        } catch(err){
            next(err)
        }
    }
    brandList = async (req, res, next) => {
       try{
        let allBrandList = await this.brand_srv.brandList()
        res.json({
                    result:allBrandList,
                    msg : "All brands are fetched.",
                    status: true
                  })
       } catch(err){
        next(err)
       }
    }
    getById = async (req, res, next) => {
        try{
            let brands = await this.brand_srv.getBrandById(req.params.id)
            res.json({
                        result:brands,
                        msg : "Brands fetched successfully.",
                        status: true
                      })
           } catch(err){
            next(err)
           }
    }
    updateBrand = async (req, res, next) => {
        try{
            let data = req.body
            // console.log(req.file)
            if(req.file) {
                data.image = req.file.filename
            }
            this.brand_srv. validationBrand(data)
           this.brand_srv.updateBrandById(data, req.params.id)
            res.json({
                result : data,
                status : true,
                msg : "Brand update successfully."
            })

        } catch(err){
            next(err)
        }
    }
    deleteById = async (req, res, next) => {
        try{
            let ack = await this.brand_srv.deleteBrandById(req.params.id)
            if(ack){
                deleteImage('brand', ack.image)
            }
            res.json({
                result : ack,
                status : true,
                msg : "Brand deleted successfully."
            })
        } catch(err){
            next(err)
        }
    }
}

module.exports = BrandController
