const BannerService = require('../services/banner.service')
const {deleteImage} = require('../helpers/function')
class BannerController {
    constructor(){
        this.banner_srv = new BannerService()
    }
    addBanner = (req, res, next) => {
        try{
            let data = req.body
            if(req.file) {
                data.image = req.file.filename
            }
            this.banner_srv.validationBanner(data)
            this.banner_srv.createBanner(data)
            res.json({
                msg : "Banner created successfully.",
                status : true,
                result : data
            })

        } catch(err){
            next(err)
        }
    }
    bannerList = async (req, res, next) => {
       try{
        let allBannerList = await this.banner_srv.bannerList()
        res.json({
                    result:allBannerList,
                    msg : "All banners are fetched.",
                    status: true
                  })
       } catch(err){
        next(err)
       }
    }
    getById = async (req, res, next) => {
        try{
            let banners = await this.banner_srv.getBannerById(req.params.id)
            res.json({
                        result:banners,
                        msg : "Banners fetched successfully.",
                        status: true
                      })
           } catch(err){
            next(err)
           }
    }
    updateBanner = async (req, res, next) => {
        try{
            let data = req.body
            // console.log(req.file)
            console.log(data)
            if(req.file) {
                data.image = req.file.filename
            } else {
                delete data.image
            }
            this.banner_srv. validationBanner(data)
           this.banner_srv.updateBannerById(data, req.params.id)
            res.json({
                result : data,
                status : true,
                msg : "Banner update successfully."
            })

        } catch(err){
            next(err)
        }
    }
    deleteById = async (req, res, next) => {
        try{
            let ack = await this.banner_srv.deleteBannerById(req.params.id)
            if(ack){
                deleteImage('banner', ack.image)
            }
            res.json({
                result : ack,
                status : true,
                msg : "Banner deleted successfully."
            })
        } catch(err){
            next(err)
        }
    }
}

module.exports = BannerController
