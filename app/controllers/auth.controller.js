const AuthService = require('../services/auth.service')
const db = require('../services/mongodb.service')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
class AuthController {
    constructor(){
        this.auth_srv = new AuthService()
    }
    login = async(req, res, next) => {
        try {
            let data = req.body;
            let validation_msg = this.auth_srv.loginValidation(data);
            if (validation_msg) {
              next({
                msg: validation_msg,
                status: 422,
              });
            } else {
               let user = await this.auth_srv.login(data)
               let token = this.auth_srv.getToken({
                id : user._id,
                user : user.name
               })

              res.json({
                result : {
                  user : user, 
                  access_token : token
                },
                status : true,
                msg : "Login successfully."
              })              
              // let selected_db = await db();
              // if (selected_db) {
              //   let user = await selected_db.collection("users").findOne({
              //     email: data.email,
              //     password: data.password,
              //   });
              //   if (user) {
              //     res.json({
              //       result: data,
              //       msg: "login successfully.",
              //     });
              //   }
              // } else {
              //   next({
              //     status: 400,
              //     msg: "credential does not match. ",
              //   });
              // }
            }
          } catch (err) {
            // next({
            //   msg: err,
            //   status: 500,
            // });
            next(err)
          }
    }
    register = async(req, res, next) => {
        try{
          let data = req.body;
          if (req.file) {
            data.image = req.file.filename;
          }
          this.auth_srv.registerValidation(data);

          data.password = bcrypt.hashSync(data.password, 10)
          this.auth_srv.registerUser(data)
          .then((succ) => {
            res.json({
              status : true,
              msg : "User registered.",
              result : data
            })
          })
          .catch((err) => {
            next({
              status : 400,
              msg : err
            })
          })
          // req.myEvent.emit("send_email", data);
    
          // try {
          //   let selected_db = await db();
          //   selected_db.collection("users").insertOne(data);
          //   res.json({
          //     result: data,
          //     status: true,
          //     msg: "Success",
          //   });
          // } catch (err) {
          //   next({
          //     msg: err,
          //     status: 500,
          //   });
          // }
        } catch(error){
          console.log(error);
          next({
            status: 422,
            msg: error,
          });
        }
    }
    socialLogin = async(req, res, next) => {
      let type = {

      }
      if(req.body.type === "fb"){
        type = {
          facebook : req.body.id
        }
      }else if(req.body.type === "google"){
        type = {
          facebook : req.body.id
        }
      }
      db()
      .then((selected_db) => {
        selected_db.collection().updateOne({
          _id: new mongoose.ObjectId(req.params.id)
        },{
          $set: {
            social : {
              facebook : type
            }
          }
        })
      })
    }
    getUserBySocail = (req, res,next) => {
      let type = {}
      if(req.body.type === "fb"){
        type = {
          facebook : req.body.id
        }
      }else if(req.body.type === "google"){
        type = {
          facebook : req.body.id
        }
      }
      
    }
}

module.exports = AuthController