const ProductService = require('../services/product.service')
const {deleteImage} = require('../helpers/function')
const slugify = require('slugify')
const { generateRandomString } = require('../config/function')
class ProductController {
    constructor(){
        this.product_srv = new ProductService()
    }
    addProduct = async (req, res, next) => {
        try{
            let data = req.body
            if(req.files) {
                let images = []
                req.files.map((item) => {
                    images.push(item.filename)
                })
                data.images = images
            }
            this.product_srv.validationProduct(data)
            
            let slug = slugify(data.title, {replacement : "-", lower : true})
            
         data.slug  = await this.product_srv.generateUniqueSlug(slug)

         if(!data.category){
            data.category = null
         } 

         if(!data.brand){
            data.brand = null
         }
         if(!data.seller){
            data.seller = null
         }
         
         let discount = {
            discount_type : "percent",
            discount_value : 0
         }

         if(data.discount_type){
            discount.discount_type = data.discount_type,
            delete data.discount_type
         }

         if(data.discount_value){
            discount.discount_value = data.discount_value,
            delete data.discount_value
         }
         data.discount = discount

         if(data.discount.discount_type === 'percent'){
             data.after_discount = data.price - data.price * data.discount.discount_value / 100 
            }else {
             data.after_discount = data.price - data.discount.discount_value
         }

         data.product_code = generateRandomString(10)

          let ack = await  this.product_srv.createProduct(data)
            res.json({
                msg : "Product created successfully.",
                status : true,
                result : data
            })

        } catch(err){
            next(err)
        }
    }
    productList = async (req, res, next) => {
       try{
        let allProductList = await this.product_srv.productList()
        res.json({
                    result:allProductList,
                    msg : "All categories are fetched.",
                    status: true
                  })
       } catch(err){
        next(err)
       }
    }
    getById = async (req, res, next) => {
        try{
            let products = await this.product_srv.getProductById(req.params.id)
            res.json({
                        result:products,
                        msg : "Products fetched successfully.",
                        status: true
                      })
           } catch(err){
            next(err)
           }
    }
    updateProduct = async (req, res, next) => {
        try{
            let data = req.body
            if(req.files) {
                let images = []
                req.files.map((item) => {
                    images.push(item.filename)
                })
                data.images = images
            }
            this.product_srv.validationProduct(data)
            
         if(!data.category){
            data.category = null
         } 

         if(!data.brand){
            data.brand = null
         }
         if(!data.seller){
            data.seller = null
         }
         
         let discount = {
            discount_type : "percent",
            discount_value : 0
         }

         if(data.discount_type){
            discount.discount_type = data.discount_type,
            delete data.discount_type
         }

         if(data.discount_value){
            discount.discount_value = data.discount_value,
            delete data.discount_value
         }
         data.discount = discount

         if(data.discount.discount_type === 'percent'){
             data.after_discount = data.price - data.price * data.discount.discount_value / 100 
            }else {
             data.after_discount = data.price - data.discount.discount_value
         }

          let ack = await  this.product_srv.updateProductById(data, req.params.id)
            res.json({
                status : true,
                result : data,
                msg : "Product updated  successfully."
            })
        } catch(err){
            next(err)
        }
    }
    deleteById = async (req, res, next) => {
        try{
            let ack = await this.product_srv.deleteProductById(req.params.id)
            if(ack){
                deleteImage('product', ack.image)
            }
            res.json({
                result : ack,
                status : true,
                msg : "Product deleted successfully."
            })
        } catch(err){
            next(err)
        }
    }
}

module.exports = ProductController
