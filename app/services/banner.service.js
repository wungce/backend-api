const BannerModel = require('../models/banner.model')

class BannerService {
    validationBanner = (data) => {
        let err_msg = {}
        if(!data.name){
            err_msg.name = "Name is required."
        }
        if(!data.status){
            err_msg.status = "Status is required."
        }else if(data.status !== 'active' && data.status !== 'inactive'){
            err_msg.status = "Status can be either active or inactive."
        }
        if(Object.keys(err_msg).length > 0){
            throw {status : 400, msg : err_msg}
        }else{
            return null
        }
    }
    createBanner = async(data) => {
        try{
            let banner = new BannerModel(data)
            let ack = await banner.save()
            if(ack){
                return banner
            }else{
                throw {status : 400, msg :"Error creating banner."}
            }
        } catch(err){
            throw err
        }
    }
    bannerList = async() => {
        try{
            return await BannerModel.find()
        } catch(err){
            throw {msg : "Banner does not store.", status : 422}
        }
    }
    getBannerById = async (id) => {
        return await BannerModel.findById(id);
    }
    updateBannerById = async (data, id) => {
        try{
            return await BannerModel.findByIdAndUpdate(id, {
                $set : data
            })
        } catch(err){
            throw {status : 400, msg : err}

        }
    }
    deleteBannerById = async (id) => {
        try{
            let ack =  await BannerModel.findByIdAndRemove(id)
            if(ack){
                return ack
            }else{
                throw "Banner already delete."
            }
        } catch(err){
            throw {status : 500, msg : err}
        }
    }
}

module.exports = BannerService