const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient
const CONFIG = require('../config/config')
const db = () => {
    return new Promise((res, rej) => {
        MongoClient.connect(CONFIG.DB_URL, (err, client) => {
            if(err){
                rej(err)
            }else{
                console.log("Db is connected")
                const selected_db = client.db(CONFIG.DB_NAME)
                res(selected_db)
            }
        })
    })

}

module.exports = db