const fs = require('fs')
const multer = require('multer')

const myStorage = multer.diskStorage(
    {
        destination : (req, file, cb) => {
            let path = 'public/uploads'
               if(req.dir){
                   path = req.dir
               }
               if(!fs.existsSync(path)){
                   try{
                       fs.mkdirSync(path, {
                           recursive : true
                       })
                   } catch(error){
                       console.log("File does not create")
                   }
            }
            cb(null, path)
        },
        filename: (req, file, cb) => {
            let filename = Date.now()+"-"+file.originalname
            cb(null, filename)
        }
    }
)

const filterImage = (req, file, cb) => {
    let exts = file.originalname.split('.')
    let ext = exts.pop()
    let allowed = ['jpg', 'png', 'jpeg', 'gif', 'svg', 'webp', 'bmp']
    if(allowed.includes(ext.toLowerCase())){
        cb(null, true)
    }else{
        cb({
            msg : "Unsupported file"
        },null)
    }
}

const uploader = multer(
    {
        storage : myStorage,
        fileFilter : filterImage
    }
)

module.exports = uploader