const fs = require('fs')
const deleteImage = (path, image) => {
    let full_path = "public/uploads/"+path
    try{
        if(image){
           let result = fs.rmSync(full_path+'/'+image)
            return result
        }
    } catch(err){
        throw {status : 400, msg : err }
    }
}

module.exports = {
    deleteImage
}