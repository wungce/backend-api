const mongoose = require('mongoose')
const {StatusSchema} = require('./common/status.schema')
const BannerSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        unique : true
    },
    link : {
        type : String,
        default : null
    },
    status : StatusSchema,
    image : {
        type : String,
        default : null
    }
},{
    timestamps : true,
    autoCreate: true,
    autoIndex: true
})
const BannerModel = mongoose.model("Banner",BannerSchema)
module.exports = BannerModel
