const mongoose = require('mongoose')
const {StatusSchema} = require('./common/status.schema')
const BrandSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        unique : true
    },
    status : {...StatusSchema, default : "active"},
    image : {
        type : String,
        default : null
    }
},{
    timestamps : true,
    autoCreate: true,
    autoIndex: true
})
const BrandModel = mongoose.model("Brand",BrandSchema)
module.exports = BrandModel