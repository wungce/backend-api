const mongoose = require('mongoose');
const {StatusSchema} = require('./common/status.schema');
const AddressSchema = new mongoose.Schema({
    name : String,
    street_name : String,
    house_no : Number
})
const UserSchema = new mongoose.Schema({
    name: {
        type : String,
        required : [true, "Name is required."]
    },
    email : {
        type : String,
        required : [true, "Email is required."],
        validate :{
            validator : (em) =>{
                return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(em);
            },
            message : props => `${props.value} is not valid email`
        }
    },
    password : {
        type : String,
        required : [true, "Password is required."],
        unique: [true, "Password is unique"],
    },
    image : {
        type : String,
        default : null
    },
    role: {
        type : String,
        enum : ["admin", "seller", "customer"],
        default : "customer"
    },
    status : StatusSchema,
    phone : {
        type : String,
        validate: {
            validator: function(v) {
              return /\d{3}-\d{3}-\d{4}/.test(v);
            },
            message: props => `${props.value} is not a valid phone number!`
          },
    },
                           
    // address : {
    //     type : String          two way to write declare address
    // }
    address : {
        billing :AddressSchema,
        shipping :AddressSchema
    }

},{
    timestamps : true,
    autoIndex : true,
    autoCreate : true
})
const UserModel = mongoose.model('User', UserSchema)
module.exports = UserModel